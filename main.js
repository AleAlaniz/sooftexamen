const express = require('express');
const app = express();
const bodyParser    = require('body-parser');
const http = require('http');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.set('port',process.env.PORT || 90);

let restaurantes = [];

app.get("/obtenerRestaurantes",(req,res)=>{
    res.send(restaurantes);
})

app.get("/obtenerRestaurantes/:type",(req,res)=>{
    let restaurantesFiltrados = [];
    restaurantes.forEach(e=>{
        if(e.kindOfRestaurant == req.params.type){
            restaurantesFiltrados.push(e);
        }
    })
    res.send(restaurantesFiltrados)
})

app.post("/agregarRestaurante",(req,res)=>{
    restaurantes.forEach(e => {
        if(e.name == req.body.name){
            res.send("error").status(401);
        }
    });
    restaurantes.push(req.body)
    res.send("ok").status(201);
})



http.createServer(app).listen(app.get('port'),function() {
    console.log('Escuchando en el puerto '+app.get('port'));
});